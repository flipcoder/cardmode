# cardmode

Trello in your text editor.
Vim plug-in: [vim-cardmode](https://github.com/flipcoder/vim-cardmode)

Open-source under MIT License. See LICENSE for details.

Copyright (c) Grady O'Connell, 2018

*Warning: Still in development.  Not yet safe for use on real data.*

## Progress

- [x] Basic json pull
- [x] .cardmode format
  - [x] boards
  - [x] lists
  - [x] cards
  - [x] checklists
  - [ ] comments
  - [ ] image links
  - [ ] attachments
- [ ] Basic push
- [ ] Proper handling of bad text (escaping)
- [ ] Expand pull features
- [ ] Expand push features
- [ ] Vim integration
- [ ] Vim .cardmode format highlighting
- [x] Vim metadata conceal
- [ ] Vim column-per-buffer view
- [?] Clobber option -- whether to track IDs of certain elements or do rewrites
- [ ] Webhooks?

